<div class="screen_centered">
    <h1>Welcome To <u><?php echo __SITE_NAME__; ?></u></h1>
    <h3><?php echo __SITE_DESCR__;?></h3>
    <br/>
    <h5>Seems you got to configure it right!<br/>Now you can start developing ;)</h5>
    <br/>
    <a href="<?php $this->path('Login');?>" class="btn btn-default btn-sm">Login</a>
</div>